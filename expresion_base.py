from expresion_no_terminal import ExpresionNoTerminal


class ExpresionBase(ExpresionNoTerminal):
    def __init__(self):
        super().__init__()

    def evaluar(self):
        return self.derecha.evaluar()
