from abc import ABC

from expresion_no_terminal import ExpresionNoTerminal


class ExpresionBinaria(ExpresionNoTerminal, ABC):
    def __init__(self):
        super().__init__()
        self.izquierda = None
