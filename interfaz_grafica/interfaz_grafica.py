from tkinter import Tk, StringVar, Button, Entry
from operadores.operadores import Operadores


class InterfazGrafica:
    __alto_boton = 3
    __ancho_boton = 11

    def __init__(self, evento_calcular, evento_calcular_fraccion):
        self.__evento_calcular = evento_calcular
        self.__evento_fraccion = evento_calcular_fraccion

        self.__operacion = ""

        self.__ventana = Tk()
        self.__ventana.title("Calculadora Cientifica")
        self.__ventana.geometry("445x433")

        self.__textoSalida = StringVar()

        self.__boton_fraccion = Button(self.__ventana, text="ⁿ⁄ₓ", width=self.__ancho_boton, height=self.__alto_boton,
                                       command=lambda: self.__evento_fraccion(self.__operacion))
        self.__boton_fraccion.place(x=1, y=85)

        self.__boton_log = Button(self.__ventana, text="log", width=self.__ancho_boton, height=self.__alto_boton,
                                  command=lambda: self.__agregar_input(Operadores.logaritmo))
        self.__boton_log.place(x=268, y=85)

        self.__boton_e = Button(self.__ventana, text="ℯ", width=self.__ancho_boton, height=self.__alto_boton,
                                command=lambda: self.__agregar_input(Operadores.e))
        self.__boton_e.place(x=357, y=85)

        self.__boton_raiz_cuadrada = Button(self.__ventana, text="√", width=self.__ancho_boton,
                                            height=self.__alto_boton,
                                            command=lambda: self.__agregar_input(Operadores.raiz_cuadrada))
        self.__boton_raiz_cuadrada.place(x=1, y=143)

        self.__boton_potencia = Button(self.__ventana, text="xⁿ", width=self.__ancho_boton, height=self.__alto_boton,
                                       command=lambda: self.__agregar_input(Operadores.potencia))
        self.__boton_potencia.place(x=90, y=143)

        self.__boton_raiz_n = Button(self.__ventana, text="ⁿ√", width=self.__ancho_boton, height=self.__alto_boton,
                                     command=lambda: self.__agregar_input(Operadores.raiz_enesima))
        self.__boton_raiz_n.place(x=179, y=143)

        self.__boton_parentesis_izq = Button(self.__ventana, text="(", width=self.__ancho_boton,
                                             height=self.__alto_boton,
                                             command=lambda: self.__agregar_input(Operadores.abrir_parentesis))
        self.__boton_parentesis_izq.place(x=268, y=143)

        self.__boton_parentesis_der = Button(self.__ventana, text=")", width=self.__ancho_boton,
                                             height=self.__alto_boton,
                                             command=lambda: self.__agregar_input(Operadores.cerrar_parentesis))
        self.__boton_parentesis_der.place(x=357, y=143)

        self.__boton_7 = Button(self.__ventana, text="7", width=self.__ancho_boton, height=self.__alto_boton,
                                command=lambda: self.__agregar_input(7))
        self.__boton_7.place(x=1, y=201)

        self.__boton_8 = Button(self.__ventana, text="8", width=self.__ancho_boton, height=self.__alto_boton,
                                command=lambda: self.__agregar_input(8))
        self.__boton_8.place(x=90, y=201)

        self.__boton_9 = Button(self.__ventana, text="9", width=self.__ancho_boton, height=self.__alto_boton,
                                command=lambda: self.__agregar_input(9))
        self.__boton_9.place(x=179, y=201)

        self.__boton_DEL = Button(self.__ventana, text="DEL", width=self.__ancho_boton, height=self.__alto_boton,
                                  command=lambda: self.__borrar_ultimo_input())
        self.__boton_DEL.place(x=268, y=201)

        self.__boton_AC = Button(self.__ventana, text="AC", width=self.__ancho_boton, height=self.__alto_boton,
                                 command=lambda: self.__limpiar_input())
        self.__boton_AC.place(x=357, y=201)

        self.__boton_4 = Button(self.__ventana, text="4", width=self.__ancho_boton, height=self.__alto_boton,
                                command=lambda: self.__agregar_input(4))
        self.__boton_4.place(x=1, y=259)

        self.__boton_5 = Button(self.__ventana, text="5", width=self.__ancho_boton, height=self.__alto_boton,
                                command=lambda: self.__agregar_input(5))
        self.__boton_5.place(x=90, y=259)

        self.__boton_6 = Button(self.__ventana, text="6", width=self.__ancho_boton, height=self.__alto_boton,
                                command=lambda: self.__agregar_input(6))
        self.__boton_6.place(x=179, y=259)

        self.__boton_multiplicar = Button(self.__ventana, text="×", width=self.__ancho_boton, height=self.__alto_boton,
                                          command=lambda: self.__agregar_input(Operadores.multiplicacion))
        self.__boton_multiplicar.place(x=268, y=259)

        self.__boton_dividir = Button(self.__ventana, text="÷", width=self.__ancho_boton, height=self.__alto_boton,
                                      command=lambda: self.__agregar_input(Operadores.division))
        self.__boton_dividir.place(x=357, y=259)

        self.__boton_1 = Button(self.__ventana, text="1", width=self.__ancho_boton, height=self.__alto_boton,
                                command=lambda: self.__agregar_input(1))
        self.__boton_1.place(x=1, y=317)

        self.__boton_2 = Button(self.__ventana, text="2", width=self.__ancho_boton, height=self.__alto_boton,
                                command=lambda: self.__agregar_input(2))
        self.__boton_2.place(x=90, y=317)

        self.__boton_3 = Button(self.__ventana, text="3", width=self.__ancho_boton, height=self.__alto_boton,
                                command=lambda: self.__agregar_input(3))
        self.__boton_3.place(x=179, y=317)

        self.__boton_sumar = Button(self.__ventana, text="+", width=self.__ancho_boton, height=self.__alto_boton,
                                    command=lambda: self.__agregar_input(Operadores.suma))
        self.__boton_sumar.place(x=268, y=317)

        self.__boton_restar = Button(self.__ventana, text="-", width=self.__ancho_boton, height=self.__alto_boton,
                                     command=lambda: self.__agregar_input(Operadores.resta))
        self.__boton_restar.place(x=357, y=317)

        self.__boton_0 = Button(self.__ventana, text="0", width=self.__ancho_boton, height=self.__alto_boton,
                                command=lambda: self.__agregar_input(0))
        self.__boton_0.place(x=1, y=375)

        self.__boton_coma = Button(self.__ventana, text=",", width=self.__ancho_boton, height=self.__alto_boton,
                                   command=lambda: self.__agregar_input(Operadores.separador_decimal))
        self.__boton_coma.place(x=90, y=375)

        self.__boton_pi = Button(self.__ventana, text="π", width=self.__ancho_boton, height=self.__alto_boton,
                                 command=lambda: self.__agregar_input(Operadores.pi))
        self.__boton_pi.place(x=179, y=375)

        self.__boton_porcentaje = Button(self.__ventana, text="%", width=self.__ancho_boton, height=self.__alto_boton,
                                         command=lambda: self.__agregar_input(Operadores.porcentaje))
        self.__boton_porcentaje.place(x=268, y=375)

        self.__boton_igual = Button(self.__ventana, text="=", width=self.__ancho_boton, height=self.__alto_boton,
                                    command=lambda: self.__evento_calcular(self.__operacion))
        self.__boton_igual.place(x=357, y=375)

        self.__salida = Entry(self.__ventana, font=("Arial", 20, "bold"), textvariable=self.__textoSalida, width=22,
                              bd=20, insertwidth=5, bg="powder blue", justify="right")
        self.__salida.pack()

    def __agregar_input(self, input):
        self.__operacion += str(input)
        self.__textoSalida.set(self.__operacion)

    def __borrar_ultimo_input(self):
        if len(self.__operacion) == 0:
            pass

        self.__operacion = self.__operacion[:-1]
        self.__textoSalida.set(self.__operacion)

    def __limpiar_input(self):
        self.__operacion = ""
        self.__textoSalida.set(self.__operacion)

    def mostrar(self):
        self.__ventana.mainloop()

    def mostrar_resultado(self, resultado):
        self.__operacion = str(resultado)
        self.__textoSalida.set(self.__operacion)

    def mostrar_error(self):
        self.__operacion = ""
        self.__textoSalida.set("Error")
