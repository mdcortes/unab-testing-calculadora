from expresion_binaria import ExpresionBinaria


class Dividir(ExpresionBinaria):
    def __init__(self):
        super().__init__()

    def evaluar(self):
        return self.izquierda.evaluar() / self.derecha.evaluar()
