from fractions import Fraction


class Fraccion:
    def __init__(self, factor1):
        self.factor1 = factor1

    def evaluar(self):
        return Fraction(self.factor1)
