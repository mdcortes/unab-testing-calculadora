from expresion_binaria import ExpresionBinaria
from math import log


class Logaritmo(ExpresionBinaria):
    def __init__(self):
        super().__init__()

    def evaluar(self):
        return log(self.derecha.evaluar(), self.izquierda.evaluar())
