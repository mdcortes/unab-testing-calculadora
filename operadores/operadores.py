class Operadores:
    potencia = "^"
    multiplicacion = "×"
    division = "÷"
    suma = "+"
    resta = "-"
    logaritmo = "log"
    raiz_enesima = "root"
    porcentaje = "%"
    abrir_parentesis = "("
    cerrar_parentesis = ")"
    raiz_cuadrada = "√"

    separador_decimal = "."

    e = "e"
    pi = "π"

    operadores_binarios = [potencia, multiplicacion, division, suma, resta, logaritmo, raiz_enesima, porcentaje]
    operadores_aceptan_multiplicacion_implicita = [abrir_parentesis, raiz_cuadrada]
