from expresion_binaria import ExpresionBinaria


class Porcentaje(ExpresionBinaria):
    def __init__(self):
        super().__init__()

    def evaluar(self):
        return 100 * self.izquierda.evaluar() / self.derecha.evaluar()
