from expresion_no_terminal import ExpresionNoTerminal
from math import sqrt


class RaizCuadrada(ExpresionNoTerminal):
    def evaluar(self):
        return sqrt(self.derecha.evaluar())
