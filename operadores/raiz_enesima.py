from expresion_binaria import ExpresionBinaria


class RaizEnesima(ExpresionBinaria):
    def __init__(self):
        super().__init__()

    def evaluar(self):
        return self.derecha.evaluar() ** (1/self.izquierda.evaluar())
