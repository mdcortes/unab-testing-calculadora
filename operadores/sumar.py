from expresion_binaria import ExpresionBinaria


class Sumar(ExpresionBinaria):
    def __init__(self):
        super().__init__()

    def evaluar(self):
        return self.izquierda.evaluar() + self.derecha.evaluar()
