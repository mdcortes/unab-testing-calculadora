from expresion import Expresion
from math import pi


class Pi(Expresion):
    def evaluar(self):
        return pi
