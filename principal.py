# -*- encoding :utf-8 -*-
from interfaz_grafica.interfaz_grafica import InterfazGrafica
from procesador_expresion import ProcesadorExpresion

interfaz_grafica = None


def calcular(expresion):
    operacion_procesada = ProcesadorExpresion.procesar(expresion)

    try:
        interfaz_grafica.mostrar_resultado(operacion_procesada.evaluar())

    except:
        interfaz_grafica.mostrar_error()


def calcular_fraccion(expresion):
    try:
        interfaz_grafica.mostrar_resultado(ProcesadorExpresion.procesar_fraccion(expresion))

    except:
        interfaz_grafica.mostrar_error()


if __name__ == '__main__':
    interfaz_grafica = InterfazGrafica(calcular, calcular_fraccion)
    interfaz_grafica.mostrar()
