import re

from expresion_base import ExpresionBase
from expresion_binaria import ExpresionBinaria
from expresion_no_terminal import ExpresionNoTerminal
from numero import Numero
from operadores.dividir import Dividir
from operadores.e import E
from operadores.fraccion import Fraccion
from operadores.logaritmo import Logaritmo
from operadores.multiplicar import Multiplicar
from operadores.operadores import Operadores
from operadores.porcentaje import Porcentaje
from operadores.potencia import Potencia
from operadores.raiz_cuadrada import RaizCuadrada
from operadores.raiz_enesima import RaizEnesima
from operadores.restar import Restar
from operadores.sumar import Sumar
from pi import Pi


class ProcesadorExpresion:
    @staticmethod
    def procesar(expresion):
        if expresion == "":
            return Numero(0)

        expresion = expresion.replace("/", Operadores.division)

        por_procesar = expresion
        siguiente_token = ProcesadorExpresion.__leer_siguiente_token(por_procesar)

        expresion_base = ExpresionBase()
        expresion_actual = expresion_base

        padres_expresion = dict()
        padres_expresion[expresion_base] = None

        while siguiente_token:
            if siguiente_token in Operadores.operadores_aceptan_multiplicacion_implicita:
                if not isinstance(expresion_actual, ExpresionNoTerminal):
                    expresion_actual = ProcesadorExpresion.__agregar_multiplicador_implicito(padres_expresion,
                                                                                             expresion_actual)

                expresion_operador = None

                if siguiente_token == Operadores.raiz_cuadrada:
                    expresion_operador = RaizCuadrada()

                elif siguiente_token == Operadores.abrir_parentesis:
                    contador_parentesis = 1
                    i = 0

                    while i < len(por_procesar) and contador_parentesis != 0:
                        siguiente_char = por_procesar[i+1]

                        if siguiente_char == Operadores.abrir_parentesis:
                            contador_parentesis += 1

                        elif siguiente_char == Operadores.cerrar_parentesis:
                            contador_parentesis -= 1

                        i += 1

                    expresion_operador = ProcesadorExpresion.procesar(por_procesar[1:i])

                    siguiente_token = por_procesar[0:i+1]

                expresion_actual.derecha = expresion_operador
                padres_expresion[expresion_operador] = expresion_actual
                expresion_actual = expresion_operador

            elif siguiente_token in Operadores.operadores_binarios:
                expresion_operador_binario = ProcesadorExpresion.__obtener_operador_binario(siguiente_token)

                operador_anterior = padres_expresion[expresion_actual]

                if isinstance(operador_anterior, ExpresionBinaria):
                    if ProcesadorExpresion.__tiene_prioridad(operador_anterior, expresion_operador_binario):
                        ProcesadorExpresion.__reordenar_por_prioridad(padres_expresion, operador_anterior,
                                                                      expresion_operador_binario)

                    else:
                        ProcesadorExpresion.__agregar_expresion_a_terminal(padres_expresion, expresion_actual,
                                                                           expresion_operador_binario)

                else:
                    ProcesadorExpresion.__agregar_expresion_a_terminal(padres_expresion, expresion_actual,
                                                                       expresion_operador_binario)

                expresion_actual = expresion_operador_binario

            elif siguiente_token in [Operadores.pi, Operadores.e]\
                    or siguiente_token.replace(Operadores.separador_decimal, "").isdigit():
                if not isinstance(expresion_actual, ExpresionNoTerminal):
                    expresion_actual = ProcesadorExpresion.__agregar_multiplicador_implicito(padres_expresion,
                                                                                             expresion_actual)
                expresion_terminal = None

                if siguiente_token == Operadores.pi:
                    expresion_terminal = Pi()

                elif siguiente_token == Operadores.e:
                    expresion_terminal = E()

                elif siguiente_token.isdigit():
                    expresion_terminal = Numero(int(siguiente_token))

                elif siguiente_token.count(Operadores.separador_decimal) == 1:
                    expresion_terminal = Numero(float(siguiente_token))

                expresion_actual.derecha = expresion_terminal
                padres_expresion[expresion_terminal] = expresion_actual
                expresion_actual = expresion_terminal

            por_procesar = por_procesar[len(siguiente_token):]
            siguiente_token = ProcesadorExpresion.__leer_siguiente_token(por_procesar)

        return expresion_base

    @staticmethod
    def procesar_fraccion(expresion):
        fraccion = Fraccion(expresion)
        return fraccion.evaluar()

    @staticmethod
    def __leer_siguiente_token(expresion):
        if len(expresion) == 0:
            return None

        i = 0
        while i < len(expresion) and (re.search("\d", expresion[i]) is not None
                                      or expresion[i] == Operadores.separador_decimal):
            i += 1

        if i > 0:
            return expresion[0: i]

        elif expresion[0] in [Operadores.e, Operadores.raiz_cuadrada, Operadores.potencia, Operadores.abrir_parentesis,
                              Operadores.cerrar_parentesis, Operadores.multiplicacion, Operadores.division,
                              Operadores.suma, Operadores.resta, Operadores.pi, Operadores.porcentaje]:
            return expresion[0]

        elif expresion[0:3] == Operadores.logaritmo:
            return Operadores.logaritmo

        elif expresion[0:4] == Operadores.raiz_enesima:
            return Operadores.raiz_enesima

    @staticmethod
    def __agregar_expresion_a_terminal(padres_expresion, expresion_final, expresion):
        padre = padres_expresion[expresion_final]
        padre.derecha = expresion
        expresion.izquierda = expresion_final

        padres_expresion[expresion] = padre
        padres_expresion[expresion_final] = expresion

    @staticmethod
    def __obtener_operador_binario(operador):
        if operador == Operadores.potencia:
            return Potencia()
        elif operador == Operadores.multiplicacion:
            return Multiplicar()
        elif operador == Operadores.division:
            return Dividir()
        elif operador == Operadores.suma:
            return Sumar()
        elif operador == Operadores.resta:
            return Restar()
        elif operador == Operadores.raiz_enesima:
            return RaizEnesima()
        elif operador == Operadores.logaritmo:
            return Logaritmo()
        elif operador == Operadores.porcentaje:
            return Porcentaje()

    @staticmethod
    def __agregar_multiplicador_implicito(padres_expresion, expresion_actual):
        expresion_multiplicar = Multiplicar()
        ProcesadorExpresion.__agregar_expresion_a_terminal(padres_expresion, expresion_actual,
                                                           expresion_multiplicar)
        return expresion_multiplicar

    @staticmethod
    def __tiene_prioridad(operador_1, operador_2):
        if isinstance(operador_1, (Potencia, RaizCuadrada, RaizEnesima, Logaritmo)):
            return isinstance(operador_2, (Sumar, Restar, Multiplicar, Dividir))

        elif isinstance(operador_1, (Multiplicar, Dividir)):
            return isinstance(operador_2, (Sumar, Restar))

        else:
            return False

    @staticmethod
    def __reordenar_por_prioridad(padres_expresion, expresion_actual, expresion_nueva):
        padre_expresion_actual = padres_expresion[expresion_actual]

        while isinstance(padre_expresion_actual, ExpresionBinaria)\
                and ProcesadorExpresion.__tiene_prioridad(padre_expresion_actual, expresion_nueva):
            expresion_actual = padre_expresion_actual
            padre_expresion_actual = padres_expresion[padre_expresion_actual]

        padre_expresion_actual.derecha = expresion_nueva
        padres_expresion[expresion_nueva] = padre_expresion_actual

        expresion_nueva.izquierda = expresion_actual
        padres_expresion[expresion_actual] = expresion_nueva
